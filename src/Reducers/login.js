const initialState = {
    username : '',
    password: '',
    loading: '',
    success: '',
    failure: ''
};

export const loginReducer = (state=initialState, action={})=>{
    switch(action.type){
        case 'CHANGE_SEARCH_FIELD':
            return {...state, searchField: action.payload}
        default:
            return state;
    }
};