import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import User from './Containers/User/UserLogin';
import AdminLogin from './Containers/Admin/AdminLogin';
import CookerLogin from './Containers/Cooker/CookerLogin';

import UserDashboard from './Containers/User/UserDashboard/UserDashboard';
import AdminDashboard from './Containers/Admin/AdminDashboard/AdminDashboard';
import CookerDashboard from './Containers/Cooker/CookerDashboard/CookerDashboard';
import Error from './Components/Error/Error';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/' component={User} exact/>
          <Route path='/admin' component={AdminLogin} exact/>
          <Route path='/cooker' component={CookerLogin} exact/>
          <Route path='/user/dashboard' component={UserDashboard}/>
          <Route path='/admin/dashboard' component={AdminDashboard}/>
          <Route path='/cooker/dashboard' component={CookerDashboard}/>
          <Route component={Error}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
// <Route path='/home' component={Login} exact/>
// <NavLink to="url" />