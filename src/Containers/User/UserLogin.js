import React, { Component } from 'react';
import Login from '../../Components/Login/Login.component';
import Boy from '../../Images/boy.png';
import { withRouter } from 'react-router-dom';

class User extends Component{

    constructor() {
        super()
        this.state = {
          username: '',
          password: ''
        }
      }

    changeUsername = (username) => {
        this.setState({username})
    }  

    changePassword = (password) => {
        this.setState({password})
    }
    onLogin = () => {
        if(this.state.username.length > 5 && this.state.password.length > 5){
            this.props.history.push('/user/dashboard');
        }else{
            alert('Usuario y contraseña no validos')
        }
    }

    render() {
      return (
        <div>
          <Login userType={'Padres'} image={Boy} passwordChanged={this.changePassword.bind(this)} userChanged={this.changeUsername.bind(this)} login={this.onLogin}/>
        </div>
      )
    }
}

export default withRouter(User);
