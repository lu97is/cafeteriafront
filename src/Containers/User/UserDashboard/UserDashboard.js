import React, { Component } from 'react';
import DashboardLeft from '../../../Components/DashboardLeft/DashboardLeft';
import { Row, Col} from 'react-materialize';
import Boy from '../../../Images/boy.png';

class UserDashboard extends Component{
    render() {
      return (
        <Row>
            <Col m={4}>
                <DashboardLeft 
                username='Padre' 
                type='Usuario'
                l1img='group'
                l1text='Hijo(s)'
                l2img='calendar_today'
                l2text='Menu'
                l3img='grade'
                l3text='Estatus'
                img={Boy}
                />
            </Col>

            <Col m={8}>
            
            </Col>
        </Row>
      )
    }
}

export default UserDashboard;