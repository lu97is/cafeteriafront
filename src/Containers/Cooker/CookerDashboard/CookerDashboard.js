import React, { Component } from 'react';
import DashboardLeft from '../../../Components/DashboardLeft/DashboardLeft';
import { Row, Col} from 'react-materialize';
import Cooker from '../../../Images/cooking.png';
import Title from '../../../Components/Title/Title';
import DashboardRightCooker from '../../../Components/DashboardRightCooker/DashboardRightCooker';
import Inventary from '../../../Components/DashboardRightCooker/Inventary';

class CookerDashboard extends Component{
    constructor() {
        super();
        this.state = {
            selectedCooker: 1
        }
    }


    changeTab(e) {
        switch (e) {
            case 'Menu':
                this.setState({ selectedCooker: 1 })
                break;
            case 'Inventario':
                this.setState({ selectedCooker: 2 })
                break;
            case 'Reporte':
                this.setState({ selectedCooker: 3 })
                break;
            case 'Salir':
                this.setState({ selectedCooker: 4 })
                break;

            default:
                break;
        }
    }


    selectedTab = () => {
        if (this.state.selectedCooker === 1) {
            return (
                <div>
                    <Title title={'Menu'} />
                    <DashboardRightCooker />
                </div>
            )
        } else if (this.state.selectedCooker === 2) {
            return (
                <div>
                    <Title title={'Inventario'} />
                    <Inventary />
                </div>
            )
        }
        else if (this.state.selectedCooker === 3) {
            return (
                <div>
                    <Title title={'Reporte'} />
                    <h1>fds</h1>
                </div>
            )
        }
        //Sera eliminado
        else {
            return (
                <div>
                    <Title title={'Salir'} />
                    <h1>fds</h1>
                </div>
            )
        }
    }
    render() {
      return (
        <Row>
            <Col m={4}>
                <DashboardLeft 
                selectedItem={(e) => this.changeTab(e)}
                username='Cooker' 
                type='Cocinero'
                l1img='calendar_today'
                l1text='Menu'
                l2img='all_inbox'
                l2text='Inventario'
                l3img='attach_file'
                l3text='Reporte'
                img={Cooker}
                />
            </Col>

            <Col m={8}>
            {this.selectedTab()}
            </Col>
        </Row>
      )
    }
}

export default CookerDashboard;