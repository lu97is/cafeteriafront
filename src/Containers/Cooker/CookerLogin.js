import React, { Component } from 'react';
import Login from '../../Components/Login/Login.component';
import Cooker from '../../Images/cooking.png';

class CookerLogin extends Component{

    constructor() {
        super()
        this.state = {
          cookerUsername: '',
          cookerPassword: ''
        }
      }

    changeUsername = (username) => {
        this.setState({cookerUsername:username})
    }  

    changePassword = (password) => {
        this.setState({cookerPassword:password})
    }
    onLogin = () => {
        if(this.state.cookerUsername.length > 5 && this.state.cookerPassword.length > 5){
            this.props.history.push('/cooker/dashboard');
        }else{
            alert('Usuario y contraseña no validos')
        }
    }

    render() {
      return (
        <div>
            <Login userType={'Cocinero'} image={Cooker} passwordChanged={this.changePassword.bind(this)} userChanged={this.changeUsername.bind(this)} login={this.onLogin}/>
        </div>
      )
    }
}

export default CookerLogin;
