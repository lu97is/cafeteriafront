import React, { Component } from 'react';
import DashboardLeft from '../../../Components/DashboardLeft/DashboardLeft';
import { Row, Col } from 'react-materialize';
import Admin from '../../../Images/manager.png';
import DashboardRight from '../../../Components/DashboardRight/DashboardRight';
import Payment from '../../../Components/DashboardRight/Payments';
import Title from '../../../Components/Title/Title';

class AdminDashboard extends Component {


    constructor() {
        super();
        this.state = {
            selected: 1
        }
    }


    changeTab(e) {
        switch (e) {
            case 'Personal':
                this.setState({ selected: 1 })
                break;
            case 'Pagos':
                this.setState({ selected: 2 })
                break;
            case 'Reportes':
                this.setState({ selected: 3 })
                break;
            case 'Salir':
                this.setState({ selected: 4 })
                break;

            default:
                break;
        }
    }


    selectedTab = () => {
        if (this.state.selected === 1) {
            return (
                <div>
                    <Title title={'Alumnos'} />
                    <DashboardRight />
                </div>
            )
        } else if (this.state.selected === 2) {
            return (
                <div>
                    <Title title={'Pagos'} />
                    <Payment />
                </div>
            )
        }
        else if (this.state.selected === 3) {
            return (
                <div>
                    <Title title={'Reportes'} />
                    <h1>fds</h1>
                </div>
            )
        }
        //Sera eliminado
        else {
            return (
                <div>
                    <Title title={'Salir'} />
                    <h1>fds</h1>
                </div>
            )
        }
    }

    render() {
        return (
            <Row>
                <Col m={4}>
                    <DashboardLeft
                        selectedItem={(e) => this.changeTab(e)}
                        username='Administrador'
                        type='administrador'
                        l1img='group'
                        l1text='Personal'
                        l2img='payment'
                        l2text='Pagos'
                        l3img='attach_file'
                        l3text='Reportes'
                        img={Admin}
                    />
                </Col>

                <Col m={8}>

                    {this.selectedTab()}

                </Col>
            </Row>
        )
    }
}

export default AdminDashboard;