import React, { Component } from 'react';
import Login from '../../Components/Login/Login.component';
import Admin from '../../Images/manager.png';
import { withRouter } from 'react-router-dom';

class AdminLogin extends Component{

    constructor() {
        super()
        this.state = {
          adminUsername: '',
          adminPassword: ''
        }
      }

    changeUsername = (username) => {
        this.setState({adminUsername: username})
    }  

    changePassword = (password) => {
        this.setState({adminPassword: password})
    }
    onLogin = () => {
        if(this.state.adminUsername.length > 5 && this.state.adminPassword.length > 5){
            this.props.history.push('/admin/dashboard');
        }else{
            alert('Usuario y contraseña no validos')
        }
    }

    render() {
      return (
        <div>
          <Login userType={'Administrador'} image={Admin} passwordChanged={this.changePassword.bind(this)} userChanged={this.changeUsername.bind(this)} login={this.onLogin}/>
        </div>
      )
    }
}

export default withRouter(AdminLogin);
