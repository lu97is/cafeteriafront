import React from 'react';
import { Icon, Modal } from 'react-materialize';
import ModalTrigger from './ModalTrigger';

const TableBody = ({ img, firstName, lastName, id, debt, father,group,grade }) => {
    const button = {
        backgroundColor: 'white',
        border: 'none',
    };
    return (
        <tr key={id}>
            <td>
                <img src={img} alt="Student" />
            </td>
            <td><p>{firstName}</p><p>{lastName}</p></td>
            <td>
                <Modal
                    header='Detalles'
                    fixedFooter
                    trigger={<button style={button}><Icon>info</Icon></button>}>
                    <ModalTrigger 
                    firstName={firstName} 
                    lastName={lastName}
                    debt={debt} 
                    father={father} 
                    group={group}
                    grade={grade}
                    id={id}
                    />
                </Modal>
            </td>
        </tr>
    )
}

export default TableBody;