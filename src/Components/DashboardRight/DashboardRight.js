import React, {Component} from 'react';
import { Table } from 'react-materialize';
import Boy from '../../Images/boy.png';
import './DashboardRight.css';
import TableBody from './TableBody';

class DashboardRight extends Component{
    constructor() {
        super();
        this.state = {
            users: []
        }
    }

    componentWillMount() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(users => {
                this.setState({users})   
                console.log(this.state.users)   
            })
    }

 

    render() {
      return (
        <div>
        <Table hoverable >
            <thead>
                <tr>
                    <th data-field="id">Icono</th>
                    <th data-field="name">Nombre</th>
                    <th data-field="price">Info</th>
                </tr>
            </thead>

            <tbody className='tableContainer'>
                {
                    this.state.users.map((item) =>
                    {
                       return (
                           <TableBody 
                           img={Boy} 
                           key={item.id}
                           id={item.id} 
                           firstName={item.name} 
                           lastName={item.username} 
                           debt={item.phone}
                           father={item.website}
                           group={item.company.bs}
                           grade={item.company.catchPhrase}
                           />
                           )
                    })
                }
            </tbody>
        </Table>

        </div>
      )
    }
}

export default DashboardRight;


                    