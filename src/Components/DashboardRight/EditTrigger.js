import React, { Component } from 'react';
import { Input, Button } from 'react-materialize';
import Boy from '../../Images/boy.png';
import './EditTrigger.css';

class EditTrigger extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            lastName: '',
            group: '',
            grade: '',
            debt: '',
            father: ''
        }
    }

    componentDidMount() {
        fetch(`https://jsonplaceholder.typicode.com/todos/${this.props.id}`)
            .then(response => response.json())
            .then(json => this.setState({ title: json.title }))

    }

    fieldChanged(e) {
        switch (e.id) {
            case 'NameEdit':
                this.setState({ name: e.value })
                break
            case 'LastNameEdit':
                this.setState({ lastName: e.value })
                break
            case 'GroupEdit':
                this.setState({ group: e.value })
                break
            case 'GradeEdit':
                this.setState({ grade: e.value })
                break
            case 'FatherEdit':
                this.setState({ father: e.value })
                break
            case 'DebtEdit':
                this.setState({ debt: e.value })
                break
            default:
        }
    }

    render() {
        return (
            <div>
                <div className='leftTrigger'>
                    <img src={Boy} alt="Profile" />
                    <ul className='listTrigger'>
                        <Input type='text' id='NameEdit' label="Nombre"
                            onChange={(e) => this.fieldChanged(e.target)} />
                        <Input type='text' id='LastNameEdit' label="Apellido"
                            onChange={(e) => this.fieldChanged(e.target)} />
                        <Input type='text' id='GroupEdit' label="Grupo"
                            onChange={(e) => this.fieldChanged(e.target)} />
                        <Input type='text' id='GradeEdit' label="Grado"
                            onChange={(e) => this.fieldChanged(e.target)} />
                        <Input type='text' id='FatherEdit' label="Padre responsable"
                            onChange={(e) => this.fieldChanged(e.target)} />
                        <Input type='text' id='DebtEdit' label="Deuda"
                            onChange={(e) => this.fieldChanged(e.target)} />
                    </ul>
                </div>
                <div className='buttonTrigger'>
                    <Button onClick={((e) => console.log(this.state))} className='blue'>
                        Guardar
            </Button>

                </div>
            </div>
        )
    }
}

export default EditTrigger;
