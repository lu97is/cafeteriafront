import React from 'react';
import { Button, Modal } from 'react-materialize';
import Boy from '../../Images/boy.png';
import './ModalTrigger.css';
import EditTrigger from './EditTrigger';

const ModalTrigger = ({ firstName, lastName, group, grade, father, debt, id }) => {
    return (
        <div>

            <div className='leftTrigger'>
                <img src={Boy} alt="Profile" />
                <ul className='listTrigger'>
                    <li>Nombre:{firstName}{lastName} </li>
                    <li>Grado:{grade} </li>
                    <li>Grupo:{group} </li>
                    <li>Padre responsable:{father} </li>
                    <li>Deuda:{debt}{id} </li>
                </ul>
            </div>
            <div className='buttonTrigger'>
                <Modal
                    header='Editar'
                    trigger={<Button className='blue btnT'>Actualizar</Button>}>
                    <EditTrigger id={id} />
                </Modal>
                <Modal
                    header='Eliminar'
                    trigger={<Button className='red btnT'>Dar de baja</Button>}>
                    Esta acción no se podra deshacer. <br/>
                    <Button className='red btnT'>Confirmar</Button>
                </Modal>

            </div>
        </div>
    )
}

export default ModalTrigger;