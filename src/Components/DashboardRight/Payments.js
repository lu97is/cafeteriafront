import React, { Component } from 'react';
import { Input, Button, Card, Row } from 'react-materialize';
import './EditTrigger.css';
import {db} from './Students';
class Payments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            studentName: '',
            studentGrade: '1',
            studentGroup: 'A',
            studentPayment: ''
        }
    }

    componentDidMount() {
        fetch(`https://jsonplaceholder.typicode.com/todos/${this.props.id}`)
            .then(response => response.json())
            .then(json => this.setState({ title: json.title }))
    }


    selectChanged(e) {
        switch (e.id) {
            case 'studentName':
                this.setState({ studentName: e.value })
                break
            case 'studentGroup':
                this.setState({ studentGroup: e.value })
                break
            case 'studentGrade':
                this.setState({ studentGrade: e.value })
                break
            case 'studentPayment':
                this.setState({ studentPayment: e.value })
                break
            default:
        }
    }

    students = () => {
        let students = [];
        let studentsFiltered = db.filter( el => {
            return el.grade === this.state.studentGrade && el.group === this.state.studentGroup
        });
        for ( let i = 0; i < studentsFiltered.length; i++){
            students.push(<option key={studentsFiltered[i].id}>{studentsFiltered[i].name}</option>)
        }
        return students;
    }

    render() {
        return (
            <div>
                <Card>
                    <div className='leftTrigger'>
                        <Row className='listTrigger'>
                            <Input id={'studentGrade'} s={12} type='select' label="Grado" defaultValue='1'
                                onChange = {((e) => this.selectChanged(e.target))}
                            >
                                <option value='1'>Primero</option>
                                <option value='2'>Segundo</option>
                                <option value='3'>Tercero</option>
                                <option value='4'>Cuarto</option>
                                <option value='5'>Quinto</option>
                                <option value='6'>Sexto</option>
                            </Input>
                            <Input id='studentGroup' s={12} type='select' label="Grupo" defaultValue='A'
                            onChange = {((e) => this.selectChanged(e.target))}
                            >
                                <option value='A'>A</option>
                                <option value='B'>B</option>

                            </Input>
                            <Input id='studentName' s={12} type='select' label="Alumno" defaultValue='A'
                            onChange = {((e) => this.selectChanged(e.target))}>
                            {
                                this.students()
                            }
                            </Input>
                            <Input id='studentPayment' s={12} type='number'  label="Pago"
                                onChange={(e) => this.selectChanged(e.target)}
                                 />
                        </Row>
                    </div>
                    <div className='buttonTrigger'>
                        <Button onClick={(() => console.log(this.state))} className='blue'>
                            Guardar
                </Button>

                    </div>

                </Card>
            </div>
        )
    }
}

export default Payments;
