import React from 'react';
import './Title.css';

const Title = ({title}) => {
    return(
        
        <div className='left-align titleContainer'>
            <h5>{title}</h5>
        </div>
    )
}

export default Title;