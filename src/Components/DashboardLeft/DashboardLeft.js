import React from 'react';
import { Card, Icon } from 'react-materialize';
import './DashboardLeft.css'

const DashboardLeft = ({img, username, type, l1img, l1text, l2img, l2text, l3img, l3text, selectedItem}) => {
    return(
        <div>
            <Card className='profileContainer'>
            <p>{type}</p>
            <p className='imageContainer'><img src={img} alt="type"/></p>
            <p>{username}</p>
            </Card>
            <div className='ListContainer'>
                <ul>
                    <li id={l1text} onClick={(e) => selectedItem(e.currentTarget.id)} ><Icon medium>{l1img}</Icon><span>{l1text}</span></li>
                    <li id={l2text} onClick={(e) => selectedItem(e.currentTarget.id)} ><Icon medium>{l2img}</Icon><span>{l2text}</span></li>
                    <li id={l3text}  onClick={(e) => selectedItem(e.currentTarget.id)}><Icon medium>{l3img}</Icon><span>{l3text}</span></li>
                    <li ><Icon medium>clear</Icon><span>Salir</span></li>
                </ul>
            </div>
        </div>
    )
}

export default DashboardLeft;