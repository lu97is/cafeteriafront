import React from 'react';
import { Col, CardPanel, Row, Input, Button, Dropdown, Icon } from 'react-materialize';
import './Login.component.css';
import {NavLink} from 'react-router-dom';

const Login = ({userType, image, login, username, password, userChanged, passwordChanged}) => {
    return (
        <div className='loginContainer'>

            <Row className='banner'>
                <Dropdown trigger={
                    <Button><Icon>list</Icon></Button>
                }>
                    <li><NavLink to='/' >Padres</NavLink></li>
                    <li><NavLink to='/admin' >Admi</NavLink></li>
                    <li><NavLink to='/cooker' >Cocina</NavLink></li>
                </Dropdown>
            </Row>
            <Row className='login'>
                <Col offset='m3' m={6} s={12}>
                    <Row>
                        <CardPanel className='white' >
                            <p>Accesso de {userType}</p>
                            <p className='imageContainer'>
                                <img src={image} alt="User type" />
                            </p>
                            <Input value={username} 
                                onChange={(e) => userChanged(e.target.value)} 
                                type="text" 
                                label="Nombre de usuario" 
                                s={12} />
                            <Input 
                            onChange={(e) => passwordChanged(e.target.value)} 
                                value={password} 
                                type="password" 
                                label="Contraseña" s={12} />
                            <p className='ButtonContainer'>
                                <Button className='button' onClick={login} >ACCEDER</Button>
                            </p>

                        </CardPanel>
                    </Row>
                </Col>
            </Row>
        </div>
    )
};

export default Login;