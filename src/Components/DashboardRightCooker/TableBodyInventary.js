import React from 'react';

const TableBodyInventary = ({ img, foodName, id, quantity }) => {
    const add = {
        backgroundColor: 'green',
        border: 'none',
        fontSize: '22px',
        color: 'white'
    };
    const less = {
        backgroundColor: 'red',
        border: 'none',
        marginLeft: '10px',
        fontSize: '22px',
        color: 'white'
    };
    return (
        <tr key={id}>
            <td>
                <img src={img} alt="Food" />
            </td>
            <td><p>{foodName}</p></td>
            <td>
              12  
            </td>
            <td>
                <button style={add}>+</button>
                <button style={less}>-</button>
            </td>
        </tr>
    )
}

export default TableBodyInventary;

// <Modal
//                     header='Detalles'
//                     fixedFooter
//                     trigger={<button style={button}><Icon>info</Icon></button>}>
//                     <ModalTriggerCooker  
//                         foodName={foodName}
//                         quantity={quantity}
//                         img={img}
//                         key={id}
//                     />
//                 </Modal>