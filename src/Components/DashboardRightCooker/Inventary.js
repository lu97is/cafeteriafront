import React, {Component} from 'react';
import { Table, Button } from 'react-materialize';
// import './DashboardRight.css';
import TableBodyInventary from './TableBodyInventary';

class Inventary extends Component{
    constructor() {
        super();
        this.state = {
            food: []
        }
    }

    componentWillMount() {
        fetch('https://api.edamam.com/search?q=chicken&app_id=78e4e21f&app_key=10cce8e5638fc1fb74c9637f8ca8a3da')
            .then(response => response.json())
            .then(food => {
                this.setState({food: food.hits}) 
                console.log(food.hits)  
            })
    }

 

    render() {
      return (
        <div>
        <Table hoverable >
            <thead>
                <tr>
                    <th data-field="id">Icono</th>
                    <th data-field="name">Nombre</th>
                    <th data-field="quantity">Cantidad</th>
                    <th data-field="update">Actualizar</th>

                </tr>
            </thead>

            <tbody className='tableContainer'>
                {
                    this.state.food.map((item) =>
                    {
                       return (
                           <TableBodyInventary 
                           img={item.recipe.image} 
                           key={item.recipe.shareAs}
                           foodName={item.recipe.label} 
                           quantity={item.recipe.calories}
                           id={item.recipe.shareAs}
                           />
                           )
                    })
                }
            </tbody>
        </Table>
        <Button>Añadir</Button>

        </div>
      )
    }
}

export default Inventary;


                    