import React, {Component} from 'react';
import { Table } from 'react-materialize';
// import './DashboardRight.css';
import TableBodyCooker from './TableBodyCooker';

class DashboardRightCooker extends Component{
    constructor() {
        super();
        this.state = {
            menu: []
        }
    }

    componentWillMount() {
        fetch('https://api.edamam.com/search?q=chicken&app_id=78e4e21f&app_key=10cce8e5638fc1fb74c9637f8ca8a3da')
            .then(response => response.json())
            .then(menu => {
                this.setState({menu: menu.hits}) 
                console.log(menu.hits)  
            })
    }

 

    render() {
      return (
        <div>
        <Table hoverable >
            <thead>
                <tr>
                    <th data-field="id">Icono</th>
                    <th data-field="name">Nombre</th>
                    <th data-field="price">Info</th>
                </tr>
            </thead>

            <tbody className='tableContainer'>
                {
                    this.state.menu.map((item) =>
                    {
                       return (
                           <TableBodyCooker 
                           img={item.recipe.image} 
                           key={item.recipe.shareAs}
                           foodName={item.recipe.label} 
                           calories={item.recipe.calories}
                           ingredientLines={item.recipe.ingredientLines}
                           id={item.recipe.shareAs}
                           />
                           )
                    })
                }
            </tbody>
        </Table>

        </div>
      )
    }
}

export default DashboardRightCooker;


                    