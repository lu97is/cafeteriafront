import React from 'react';
import { Icon, Modal } from 'react-materialize';
import ModalTriggerCooker from './ModalTriggerCooker';

const TableBodyCooker = ({ img, foodName, id, ingredientLines, calories }) => {
    const button = {
        backgroundColor: 'white',
        border: 'none',
    };
    return (
        <tr key={id}>
            <td>
                <img src={img} alt="Food" />
            </td>
            <td><p>{foodName}</p></td>
            <td>
                <Modal
                    header='Detalles'
                    fixedFooter
                    trigger={<button style={button}><Icon>info</Icon></button>}>
                    <ModalTriggerCooker  
                        foodName={foodName}
                        ingredientLines={ingredientLines}
                        calories={calories}
                        img={img}
                        key={id}
                    />
                </Modal>
            </td>
        </tr>
    )
}

export default TableBodyCooker;